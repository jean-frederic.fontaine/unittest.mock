from unittest.mock import Mock, MagicMock, patch, mock_open
from implementations import Banana, Monkey, Lion
from abstracts import AbcFood, AbcAnimals
import pytest


@pytest.fixture(scope="function")
def monkey():
    monkey = Monkey(energy=1, strength=1, current_position=0)
    yield monkey
    del monkey


def test_mock_no_spec(monkey):
    # Show mock is to relax.
    # It's possible to set non-existing attributes
    # It's possible to access non-existing attributes
    mocked_food = Mock()
    # monkey.eat_food(mocked_food)
    mocked_food.being_eaten.assert_called()


def test_mock_no_spec_0(monkey):
    # Show a precise raise.
    bad_banana = Banana()
    bad_banana.being_eaten = Mock(side_effect=Exception("Bad fruit."))
    monkey.move_left = Mock()
    monkey.eat_food(bad_banana)
    monkey.move_left.assert_called()


def test_mock_len(monkey):
    # difference mock/magic mock
    no_magic_monkey = Mock()
    assert len(no_magic_monkey) is not None


def test_mock_len_2(monkey):
    magic_monkey = MagicMock()
    assert len(magic_monkey) is not None


def test_mock_no_spec_1(monkey):
    # Show mock is to relax.
    # Delete to_disappear
    mocked_food = Mock()
    mocked_food.to_disapear()
    mocked_food.to_disapear.assert_called()
    mocked_food.does_not_exist = 2


def test_mock_spec_no_set():
    # It's impossible to access non-existing attributes
    mocked_apple = Mock(spec=AbcFood)
    mocked_apple.to_disappear()
    assert True


def test_mock_spec_no_set_1():
    # But it's possible to set non-existing attributes.
    fake_apple = Mock(spec=AbcFood)
    fake_apple.ultra_power = 2
    assert True


def test_mock_spec_set():
    # It's impossible to set non-existing attributes.
    fake_apple = Mock(spec_set=AbcFood)
    fake_apple.to_disappear = len
    assert fake_apple.no_exist([1, 2, 3]) == 3


def test_mock_spec_set_1(monkey):
    # IO is annoying; Mock method instead.
    added_value = 5

    def being_eaten_stub(eater: AbcAnimals):
        eater.energy += added_value

    fake_apple = Mock(spec_set=AbcFood)
    fake_apple.being_eaten = being_eaten_stub
    pre_eating_energy = monkey.energy
    monkey.eat_food(fake_apple)
    assert monkey.energy == pre_eating_energy + added_value


def test_mock_io(monkey):
    # IO is annoying; Mock method more granular.
    with patch('abstracts.open', mock_open()) as _open:
        with patch("requests.post") as log_bd:
            banana = Banana()
            monkey.eat_food(banana)
            _open.assert_called_once()
            log_bd.assert_called_once()


def test_mock_spec_set_too_relax(monkey):
    # spec_set is too relaxed; can call function with wrong parameters.
    fake_apple = Mock(spec_set=AbcFood)
    fake_apple.being_eaten(1, 1, 1)
    fake_apple.being_eaten.assert_called()


@patch(target="abstracts.AbcFood", autospec=True)
def test_patch_1(mocked_food):
    # Debug; show function arguments must be the same.
    mocked_food.being_eaten(1, 1, 1)


@patch(target="implementations.Monkey", autospec=True,  spec_set=True)
def test_patch_2(mocked_monkey):
    mocked_monkey.i_dont_exist = "Oops!"


def test_patch_3(monkey):
    # Rappeler que le décorateur at decoration time.
    with patch(target="requests.post", new_callable=print("\n Posting to DB. \n")) as mocked_post:
        with patch(target="abstracts.AbcFood._log_energy_file", new_callable=print("LOGGING TO FILE.!")) as fake_log:
            lion = Lion(1, 2)
            monkey.eat_food(lion)
            mocked_post.assert_called()
            fake_log.assert_called()
            assert not monkey.is_available()

