from abstracts import AbcFood, AbcAnimals
import requests


class Banana(AbcFood):

    def __init__(self):
        super().__init__(energy=1)

    def _being_eaten(self, eater: AbcAnimals) -> int:
        to_return = self.energy
        self.energy = 0
        eater.energy += to_return
        return to_return


class Monkey(AbcAnimals):

    def _moving_speed(self) -> int:
        return self.strength * self.energy

    def _being_eaten(self, eater: AbcAnimals):
        if eater.strength > self.strength and self.energy:
            eater.energy += self.energy
            self.energy = 0
            return

        eater._being_eaten(self)


class Lion(AbcAnimals):

    def __init__(self, energy: int, strength: int):
        super().__init__(energy, strength)
        self._make_the_world_shiver()

    def _moving_speed(self) -> int:
        return self.strength * self.energy**2

    def _being_eaten(self, eater: AbcAnimals):
        # Lion gagne tout le temps..
        self.energy += eater.energy
        eater.energy = 0

    def eat_food(self, food: AbcFood):
        self.energy += food.energy
        food.energy = 0

    def _make_the_world_shiver(self):
        requests.post(url="google.com",
                      json={"data": f"WOAARG! New lion born with {self.energy} energy !"})
