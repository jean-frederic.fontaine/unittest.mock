from abc import ABC, abstractmethod
from pathlib import Path
import requests
import logging

logger = logging.getLogger(__name__)


class AbcFood(ABC):

    LOG_FILE = Path("./log.txt")

    def __init__(self, energy: int):
        self.energy = energy

    @abstractmethod
    def _being_eaten(self, eater: "AbcAnimals") -> int:
        """Define side effects on the eater and adjust self state after being eaten."""
        pass

    def being_eaten(self, eater: "AbcAnimals"):
        self._being_eaten(eater=eater)
        self._log_energy_file()
        self._log_energy_bd("www.google.com")

    def _log_energy_file(self):
        child_class = self.__class__.__name__
        with open(self.LOG_FILE, "a") as log_file:
            log_file.write(f"{child_class} have been eaten."
                           f"Current energy is {self.energy}")

    def _log_energy_bd(self, url):
        logger.info(f"{self.__class__.__name__} is about to log to DB.")
        result = requests.post(url, json={"data": self.energy})
        return result

    def is_available(self):
        if self.energy > 0:
            return True
        return False

    # def to_disappear(self):
    #     pass
    #


class AbcAnimals(AbcFood, ABC):

    def __init__(self, energy: int, strength: int, current_position: int = 0):
        super().__init__(energy=energy)
        self.strength: int = strength
        self.current_position: int = current_position

    def eat_food(self, food: AbcFood):
        # L'effet de ce qui est mangé est la responsabilité de ce qui est mangé.
        try:
            food.being_eaten(self)
        except Exception:
            self.move_left()

    @abstractmethod
    def _moving_speed(self) -> int:
        pass

    def move_right(self):
        self.current_position += self._moving_speed()

    def move_left(self):
        self.current_position -= self._moving_speed()

    def __len__(self):
        return self.strength
